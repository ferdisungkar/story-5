from django.db import models

# Create your models here.
class ClassYear(models.Model):
    class_year = models.IntegerField()

    def __str__(self):
        return str(self.class_year)

class Friends(models.Model):
    display_name = models.CharField(max_length=30, default='anonymous')
    hobby = models.CharField(max_length=30, default='no hobby')
    favourite_food = models.CharField(max_length=30, default='no favourite')
    class_year = models.ForeignKey(ClassYear, on_delete=models.CASCADE)

    def __str__(self):
        return self.display_name