from django import forms
from .models import Friends, ClassYear

class input(forms.Form):
    display_name = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Input your name',
        'type' : 'text',
        'maxlength' : '50',
        'required' : True,
        'style' : 'border:0; width:100%;text-align:center;background:black;color:white;',
        'label' : 'Name',
    }))
    hobby = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Input your hobby',
        'type' : 'text',
        'maxlength' : '50',
        'required' : True,
        'style' : 'border:0; width:100%;text-align:center;background:black;color:white;',
        'label' : 'Hobby',
    }))
    favourite_food = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Input your favourite food or drinks',
        'type' : 'text',
        'maxlength' : '50',
        'required' : True,
        'style' : 'border:0; width:100%;text-align:center;background:black;color:white;',
        'label' : 'Favourite food or drinks',
    }))
    class_year = forms.ModelChoiceField(queryset=ClassYear.objects.all())

# class input(forms.ModelForm):
#     class Meta:
#         model = Friends
#         fields = ['display_name', 'hobby', 'favourite_food', 'class_year']

#         widgets = {
#             'display_name' : forms.TextInput(attrs={
#                 'class' : 'form-control',
#                 'placeholder' : 'Input your name',
#                 'type' : 'text',
#                 'maxlength' : '50',
#                 'required' : True,
#                 'style' : 'border:0; width:100%;text-align:center;background:black;color:white;',
#                 'label' : 'Name',
#                 }),
#             'hobby' : forms.TextInput(attrs={
#                 'class' : 'form-control',
#                 'placeholder' : 'Input your hobby',
#                 'type' : 'text',
#                 'maxlength' : '50',
#                 'required' : True,
#                 'style' : 'border:0; width:100%;text-align:center;background:black;color:white;',
#                 'label' : 'Hobby',
#                 }),
#             'favourite_food' : forms.TextInput(attrs={
#                 'class' : 'form-control',
#                 'placeholder' : 'Input your favourite food or drinks',
#                 'type' : 'text',
#                 'maxlength' : '50',
#                 'required' : True,
#                 'style' : 'border:0; width:100%;text-align:center;background:black;color:white;',
#                 'label' : 'Favourite food or drinks',
#             }),
#             'class_year' : forms.TextInput(attrs={
#                 'class' : 'form-control',
#                 'placeholder' : 'Input your class year',
#                 'type' : 'Integer',
#                 'required' : True,
#                 'style' : 'border:0; width:100%;text-align:center;background:black;color:white;',
#                 'label' : 'Class Year',
#             })
#         }