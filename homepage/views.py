from django.shortcuts import render, redirect
from .forms import input
from .models import ClassYear, Friends

# Create your views here.

def index(request):
    return render(request, 'Homepage.html')

def message(request):
    if request.method == "POST":
        form = input(request.POST)
        if (form.is_valid()):
            friend = Friends()
            year = ClassYear()
            friend.display_name = form.cleaned_data['display_name']
            friend.hobby = form.cleaned_data['hobby']
            friend.favourite_food = form.cleaned_data['favourite_food']
            friend.class_year = form.cleaned_data['class_year']
            year.class_year = form.cleaned_data['class_year']
            friend.save()
        return redirect('/message')
    else:
        form = input()
        friend = Friends.objects.all()
        year = ClassYear.objects.all()
        response = {'friend' : friend, 'year' : year, 'forms': input}
        return render(request, 'Message.html', response)